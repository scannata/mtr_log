# Prerequisites

* terminal
* python 3
* pip
* virtualenv
* git
* mtr

# Installation

* `git clone git@gitlab.com:scannata/mtr_log.git`
* `virtualenv mtr_log`
* `cd mtr_log`
* `source bin/activate` (every regular OS) or `source Scripts/activate` (windows)
* `pip install -r requirements.txt`

# Run

* `vim run_mtr.py` 
* Remove `1.1.1.1` from  `ip_list`.
* Input a list of ips for `ip_list` variable to run mtr against.
* Update `num_of_packets` to the amount of packets you want the mtr to send.
* `python run_mtr.py`
* `vim output.csv` or `nano output.csv`

# Run Tests

* `pytest out_output.py`

# Purpose

All this python project does is leverage Fabric to run mtr on a list of ips
with the number of packets specified. The ip / domain of the server is inserted
as a row in the .csv file followed by the `mtr`'s stdout (standard output). The
.csv file is named by the date and time stamp the python script. The file is
put in the same folder the script is run from.

# Yeah, but why bother?

I don't know; get off my back. Why don't you go play with traceroute or
something.
