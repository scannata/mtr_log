#!/user/bin/env python

import run_mtr
import os


def test_outut_user_specified_dir(tmpdir):
    p = tmpdir.mkdir("sub")
    print(type(p))
    run_mtr.run_mtr(['1.1.1.1'], 2, str(p))
    assert len(tmpdir.listdir()) == 1

