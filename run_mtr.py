#!/user/bin/env python

import csv
from datetime import datetime
from fabric.api import *
import os


def create_csv_row(data_list, output_path):
    """Receives a list of data to write to an output.csv file. Each index in
       the list is written as its own row in CSV.

    :param data_list: A list of data to get written to a CSV file.
    :param output_path: Path for a directory path to get written to.
    """
    if output_path is None:
        output_path = os.path.dirname(os.path.realpath(__file__))
    date = datetime.now().strftime("%Y%m%d-%H%M%S")
    output_file = os.path.join(output_path, date + ".csv")
    with open(output_file, 'w') as result_file:
        wr = csv.writer(result_file)
        for line in data_list:
            wr.writerow([line])

def run_mtr(ip_list, num_of_packets, output_path=None):
    """Receives a list of ip addresses to run mtr command with.

    :param ip_list: a list of ip to run mtr on.
    :param num_of_packets: the number of packets for the mtr to send.
    :param output_path: the absolute path for an output CSV's desination.
    """
    output_list = []
    for ip in ip_list:
        output_list.append(ip)
        command = 'mtr -r ' + ip + ' -c ' + str(num_of_packets)
        output = local(command, capture=True).rsplit("\n")

        for line in output:
            print(line)
            output_list.append(line)

    create_csv_row(output_list, output_path)

if __name__ == '__main__':
    ip_list = ['1.1.1.1']
    num_of_packets = 1
    output_path = None
    run_mtr(ip_list, num_of_packets, output_path)

